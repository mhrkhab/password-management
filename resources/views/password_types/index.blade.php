@extends('layouts.app')

@section('content')

    <div style="text-align: left;padding: 30px;color: navy">
        <a href="{{route('users.index')}}"><b>ALL USERS</b></a>
    </div>

    <form action="{{ route('passwords.types.store') }}" method="POST">
        @csrf
        <div>
            <input type="text" placeholder="enter new password type here!" style="padding: 20px;width: 700px" id="password_type" name="password_type" required autofocus>
        </div>
        <div class="d-grid mx-auto">
            <button type="submit" style="height: 20px;margin-top: 20px">REGISTER NEW PASSWORD TYPE</button>
        </div>
    </form>

    <table style="text-align: center;padding: 100px">
        <thead>
            <tr>
                <th style="padding-left: 40px">id</th>
                <th style="padding-left: 40px">password_type</th>
                <th style="padding-left: 40px">manage</th>
            </tr>
        </thead>
        <tbody>
            @foreach($passwordTypes as $passwordType)
                <tr>
                    <td style="padding-left: 40px">{{$passwordType['id']}}</td>
                    <td style="padding-left: 40px">{{$passwordType['password_type']}}</td>
                    <td style="padding-left: 40px">
                        <a style="padding-right: 30px;color: red" class="btn" href="{{route('passwords.types.destroy',[$passwordType['id']])}}"><b>DELETE</b></a>
                        <a style="color: blue" href="{{route('passwords.index',[$passwordType['id']])}}"><b>SHOW PASSWORDS</b></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
