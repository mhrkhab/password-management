@extends('layouts.app')

@section('content')

    <form action="{{ route('passwords.store') }}" method="POST">
        @csrf
        <div>
            <input type="text" placeholder="enter new password for this type here!!" style="padding: 20px;width: 700px" id="password" name="password" required autofocus>
            <input type="hidden" name="password_type_id" id="password_type_id" value="{{$password_type_id}}">
        </div>
        <div class="d-grid mx-auto">
            <button type="submit" style="height: 20px;margin-top: 20px;color: purple">CREAT NEW PASSWORD</button>
        </div>
    </form>

    <table style="text-align: center;padding: 100px">
        <thead>
        <tr>
            <th style="padding-left: 40px">id</th>
            <th style="padding-left: 40px">password</th>
            <th style="padding-left: 40px">manage</th>
        </tr>
        </thead>
        <tbody>
        @foreach($passwords as $password)
            <tr style="padding-bottom: 20px">
                <td style="padding-left: 40px">{{$password['id']}}</td>
                <td style="padding-left: 40px">{{$password['password']}}</td>
                <td style="padding-left: 40px;padding-bottom: 40px">
                    <a style="padding-right: 30px;color: red" class="btn" href="{{route('passwords.destroy',[$password['id']])}}"><b>DELETE</b></a>
                    <form action="{{ route('passwords.update',[$password['id']]) }}" method="POST">
                        @csrf
                        <div>
                            <input type="text" placeholder="update password here!!" id="password" name="password" required autofocus>
                            <button type="submit" style="height: 20px;margin-top: 20px;color: blue">UPDATE PASSWORD</button>
                        </div>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
