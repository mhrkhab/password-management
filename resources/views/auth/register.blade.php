@extends('layouts.app')

@section('content')
    <form action="{{ route('signUp') }}" method="POST">
        @csrf
        <div>
            <input type="text" placeholder="username" id="username" name="username" required autofocus>
        </div>

        <div>
            <input type="password" placeholder="Password" id="password" name="password" required>
        </div>
        <div class="d-grid mx-auto">
            <button type="submit">REGISTER</button>
        </div>
    </form>
@endsection
