@extends('layouts.app')

@section('content')
    <form action="{{ route('login') }}" method="POST">
        @csrf
        <div>
            <input type="text" placeholder="username" id="username" name="username" required autofocus>
        </div>

        <div>
            <input type="password" placeholder="Password" id="password" name="password" required>
        </div>
        <div class="d-grid mx-auto">
            <button type="submit">LOGIN</button>
        </div>
    </form>
    <br>
    <a href={{route('register')}}>Sign Up</a>
@endsection
