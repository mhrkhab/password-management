@extends('layouts.app')

@section('content')

    <form action="{{ route('users.store') }}" method="POST">
        @csrf
        <div>
            <input type="text" placeholder="username" id="username" name="username" required autofocus>
        </div>

        <div>
            <input type="password" placeholder="Password" id="password" name="password" required>
        </div>
        <div class="d-grid mx-auto">
            <button type="submit" style="color: purple">REGISTER NEW USER ADMINISTRATIVE</button>
        </div>
    </form>

    <table style="text-align: center;padding: 100px">
        <thead>
        <tr>
            <th style="padding-left: 40px">id</th>
            <th style="padding-left: 40px">username</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td style="padding-left: 40px">{{$user['id']}}</td>
                <td style="padding-left: 40px">{{$user['username']}}</td>
                <td style="padding-left: 40px">
                    <a style="padding-right: 30px;color: red" class="btn" href="{{route('users.destroy',[$user['id']])}}"><b>DELETE</b></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
