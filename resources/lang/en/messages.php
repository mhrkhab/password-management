<?php
return [
    'error' => [
        'access' => 'you don\'t have permission to access this resource',
        'ownership' => 'this resource not related to you',
        'signUp' => 'you\'re registered failed',
        'login'  => 'login failed! try again',
        'password_type' => [
            'store' => 'an error occurred in storing password type',
            'destroy' => 'an error occurred in destroying password type'
        ],
        'password' => [
            'store' => 'an error occurred in storing password',
            'destroy' => 'an error occurred in destroying password',
            'update' => 'an error occurred in updating password',
        ],
        'users' => [
            'store' => 'new user registering was failed',
            'destroy' => 'an error occurred in destroying user',
        ],
    ],
    'success' => [
        'signUp' => 'you\'re registered successfully',
        'login'  => 'you\'re logged in successfully',
        'password_type' => [
            'store' => 'password type stored successfully',
            'destroy' => 'password type destroyed successfully'
        ],
        'password' => [
            'store' => 'password stored successfully',
            'destroy' => 'password destroyed successfully',
            'update' => 'password updated successfully',
        ],
        'users' => [
            'store' => 'registered new user successfully',
            'destroy' => 'user destroyed successfully',
        ],
    ]
];
