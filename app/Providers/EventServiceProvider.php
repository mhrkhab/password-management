<?php

namespace App\Providers;

use App\Events\PasswordType\DestroyedPasswordType;
use App\Events\Users\DestroyedUser;
use App\Listeners\PasswordType\DestroyedPasswordTypeListener;
use App\Listeners\Users\DestroyedUserListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        DestroyedPasswordType::class => [
            DestroyedPasswordTypeListener::class
        ],
        DestroyedUser::class => [
            DestroyedUserListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
