<?php

namespace App\Models;
/**
 * Class PasswordType
 * @package App\Models
 *
 * @property $password_type
 */
class PasswordType extends Base
{
    use Ownership;

    protected $fillable = [
        'password_type',
        'user_id'
    ];
}
