<?php

namespace App\Models;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

/**
 * Trait Ownership
 * @package App\Models
 *
 * @property $user_id
 */
trait Ownership
{
    /**
     * @return array
     * @throws FileNotFoundException
     */
    public static function getAllMine($user_id=null)
    {
        $results = collect(self::getAll())
            ->groupBy('user_id')
            ->toArray();

        $user_id = self::getUserId($user_id);

        if (isset($results[$user_id]))
            return $results[$user_id];
        return [];
    }

    /**
     * @param $id
     * @param mixed $user_id
     * @return bool
     * @throws Exception
     */
    public static function delete($id, $user_id=null)
    {
        if (self::checkResource($id, $user_id))
            return parent::delete($id);
        return false;
    }

    /**
     * @param array $data
     * @param null $id
     * @return mixed
     * @throws Exception
     */
    public function createOrUpdate(array $data, $id=null, $user_id=null)
    {
        if (isset($id)){
            self::checkResource($id, $user_id);
        }
        return parent::createOrUpdate($data, $id);
    }

    /**
     * @param $id
     * @param null $user_id
     * @return bool
     * @throws FileNotFoundException
     */
    private static function checkResource($id, $user_id=null)
    {
        $result = self::find($id);
        if ($result->user_id != self::getUserId($user_id))
            throw new Exception('this resource not related to you');

        return true;
    }

    private static function getUserId($user_id=null)
    {
        if (!isset($user_id))
            $user_id = request()->session()->get('user_id');

        return $user_id;
    }
}
