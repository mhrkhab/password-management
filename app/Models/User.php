<?php

namespace App\Models;
/**
 * Class User
 * @package App\Models
 *
 * @property $username
 * @property $password
 */
class User extends Base
{
    protected $fillable = [
        'username',
        'password'
    ];

    public static function findByUsername($username){
        $users = collect(self::getAll())
            ->keyBy('username')
            ->toArray();
        if (isset($users[$username]))
            return (object)$users[$username];
        return null;
    }
}
