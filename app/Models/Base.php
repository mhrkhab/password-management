<?php

namespace App\Models;

use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\Filesystem;

/**
 * Class Base
 * @package App\Models
 *
 * @property $id
 */
abstract class Base
{
    protected $fillable;

    /**
     * @return false|string
     */
    public static function getTable()
    {
        return substr(
            get_called_class(),
            strrpos(get_called_class(), '\\' )+1
        );
    }

    /**
     * @param array $data
     * @param null $id
     * @return int|mixed|null
     * @throws Exception
     */
    public function createOrUpdate(array $data, $id=null)
    {
        $data = Arr::only(Arr::except($data,['id']),$this->fillable);
        if (isset($id)) {
            $data = array_merge((array)self::find($id), $data);
        }
        else{
            $id = self::getNewId();
            $data = array_merge(
                [
                    'id' => $id
                ],
                $data
            );
        }
        return self::getDisc()->put(
            self::getFileAddress($id),
            json_encode($data)
        ) ? $id : null;
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public static function delete($id)
    {
        if (self::exists($id))
            return self::getDisc()->delete(
                self::getFileAddress($id)
            );
    }

    /**
     * @return array
     * @throws FileNotFoundException
     */
    public static function getAll()
    {
        $records = array_filter(self::getDisc()->files(self::getTable()),function ($file){
            return strpos($file,'.txt');
        });
        $result = [];
        foreach ($records as $record){
            $result[] = json_decode(self::getDisc()->get($record),true);
        }
        return $result;
    }

    /**
     * @param $id
     * @return object
     * @throws FileNotFoundException
     */
    public static function find($id)
    {
        if (self::exists($id))
            return (object)json_decode(self::getDisc()->get(
                self::getFileAddress($id)
            ), true);
    }

    /**
     * @throws Exception
     */
    public static function exists($id)
    {
        if (!self::getDisc()->exists(self::getFileAddress($id)))
            throw new Exception('not found any thing');
        return true;
    }

    /**
     * @return Filesystem
     */
    private static function getDisc()
    {
        return Storage::disk('local');
    }

    /**
     * @param $pattern
     * @return string
     */
    private static function getFileAddress($pattern)
    {
        return self::getTable().'/'.$pattern.'.txt';
    }

    /**
     * @throws Exception
     */
    private static function getNewId()
    {
        $id_file = 'ID/id_counter';
        if (self::getDisc()->exists(self::getFileAddress($id_file))){
            $id = json_decode(self::getDisc()->get(
                self::getFileAddress($id_file)
            ),true)['id']+1;

            if (
                !self::getDisc()->put(self::getFileAddress($id_file),json_encode(['id' => $id]))
            ) {
                throw new Exception('an error occurred in save id');
            }

            return $id;
        }

        if (
        !self::getDisc()->put(self::getFileAddress($id_file),json_encode(['id' => 1]))
        ) {
            throw new Exception('an error occurred in save id');
        }

        return 1;
    }
}
