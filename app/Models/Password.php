<?php

namespace App\Models;
/**
 * Class Password
 * @package App\Models
 *
 * @property $password_type_id
 * @property $password
 */
class Password extends Base
{
    use Ownership;

    protected $fillable = [
        'password_type_id',
        'user_id',
        'password'
    ];

    public static function getAllMineByPasswordTypeId($password_type_id)
    {
        $items = collect(self::getAllMine())
            ->groupBy('password_type_id')
            ->toArray();
        return $items[$password_type_id] ?? [];
    }
}
