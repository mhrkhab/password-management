<?php

namespace App\Http\Middleware;

use App\Models\PasswordType;
use Closure;
use Illuminate\Http\Request;

class CheckPasswordTypeOwnershipMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $password_type_id = $request->input('password_type_id');
        if (!isset($password_type_id)){
            $password_type_id = $request->route('password_type_id');
        }
        if (isset($password_type_id)){
            /** @var PasswordType $passwordType */
            $passwordType = PasswordType::find($password_type_id);
            if ($passwordType->user_id == $request->session()->get('user_id')){
                return $next($request);
            }
        }
        $request->session()->flash('error',__('messages.error.ownership'));
        return redirect()->route('passwords.types.index');
    }
}
