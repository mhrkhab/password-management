<?php

namespace App\Http\Controllers;

use App\Http\Requests\Passwords\StorePasswordRequest;
use App\Http\Requests\Passwords\UpdatePasswordRequest;
use App\Models\Password;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Class PasswordsController
 */
class PasswordsController extends Controller
{
    /**
     * @param StorePasswordRequest $request
     * @return RedirectResponse
     */
    public function store(StorePasswordRequest $request)
    {
        $request->merge([
            'user_id' => $request->session()->get('user_id')
        ]);
        if (app(Password::class)->createOrUpdate($request->all()) != null){
            $request->session()->flash('success',__('messages.success.password.store'));
            return redirect()->back();
        }
        $request->session()->flash('error',__('messages.error.password.store'));
        return redirect()->back();
    }

    /**
     * @param UpdatePasswordRequest $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(UpdatePasswordRequest $request, $id)
    {
        if (app(Password::class)->createOrUpdate(Arr::only($request->all(),['password']),$id) != null){
            $request->session()->flash('success',__('messages.success.password.update'));
        }
        else{
            $request->session()->flash('error',__('messages.error.password.update'));
        }
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Request $request, $id)
    {
        if (Password::delete($id)){
            $request->session()->flash('success',__('messages.success.password.destroy'));
        }
        else{
            $request->session()->flash('error',__('messages.error.password.destroy'));
        }
        return redirect()->back();
    }

    /**
     * @param $password_type_id
     * @return Application|Factory|View
     */
    public function index($password_type_id)
    {
        return view('passwords.index',[
            'passwords'        => Password::getAllMineByPasswordTypeId($password_type_id),
            'password_type_id' => $password_type_id,
        ]);
    }
}
