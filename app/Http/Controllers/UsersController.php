<?php

namespace App\Http\Controllers;

use App\Events\Users\DestroyedUser;
use App\Http\Requests\Users\StoreUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function store(StoreUserRequest $request)
    {
        $request->merge(['password' => Hash::make($request->input('password'))]);

        if (app(User::class)->createOrUpdate($request->all()) != null){
            $request->session()->flash('success',__('messages.success.users.store'));
        }
        else{
            $request->session()->flash('error',__('messages.error.users.store'));
        }
        return redirect()->back();
    }

    public function destroy(Request $request,$id)
    {
        if (User::delete($id)){
            // delete user's relation entities
            event(new DestroyedUser($id));
            $request->session()->flash('success',__('messages.success.users.destroy'));
        }
        else{
            $request->session()->flash('error',__('messages.error.users.destroy'));
        }
        return redirect()->back();
    }

    public function index()
    {
        return view('users.index',[
            'users' => User::getAll()
        ]);
    }
}
