<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 * Class LaunchController
 */
class LaunchController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function launch()
    {
        return view('welcome');
    }
}
