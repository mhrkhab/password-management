<?php

namespace App\Http\Controllers;

use App\Events\PasswordType\DestroyedPasswordType;
use App\Http\Requests\PasswordTypes\StorePasswordTypeRequest;
use App\Models\PasswordType;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Exception;

/**
 * Class PasswordTypesController
 */
class PasswordTypesController extends Controller
{
    /**
     * @param StorePasswordTypeRequest $request
     * @return RedirectResponse
     */
    public function store(StorePasswordTypeRequest $request)
    {
        $request->merge([
            'user_id' => $request->session()->get('user_id')
        ]);
        if (app(PasswordType::class)->createOrUpdate($request->all()) != null){
            $request->session()->flash('success',__('messages.success.password_type.store'));
            return redirect()->back();
        }
        $request->session()->flash('error',__('messages.error.password_type.store'));
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Request $request, $id)
    {
        if (PasswordType::delete($id)){
            // delete relations
            event(new DestroyedPasswordType($id));
            $request->session()->flash('success',__('messages.success.password_type.destroy'));
        }
        else{
            $request->session()->flash('error',__('messages.error.password_type.destroy'));
        }
        return redirect()->back();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('password_types.index',[
            'passwordTypes' => PasswordType::getAllMine()
        ]);
    }
}
