<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\SignUpRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function login(LoginRequest $request)
    {
        /** @var User $user */
        $user = User::findByUsername($request->input('username'));
        if (isset($user->id) && Hash::check($request->input('password'),$user->password)){
            $request->session()->put('user_id',$user->id);
            $request->session()->flash('success',__('messages.success.login'));
            return redirect()->route('passwords.types.index');
        }
        $request->session()->flash('error',__('messages.error.login'));
        return redirect()->route('launch');
    }

    /**
     * @return Application|Factory|View
     */
    public function register()
    {
        return view('auth.register');
    }

    /**
     * @param SignUpRequest $request
     * @return RedirectResponse
     */
    public function signUp(SignUpRequest $request)
    {
        $request->merge(['password' => Hash::make($request->input('password'))]);

        if (app(User::class)->createOrUpdate($request->all()) != null){
            $request->session()->flash('success',__('messages.success.signUp'));
        }
        else{
            $request->session()->flash('error',__('messages.error.signUp'));
        }
        return redirect()->route('launch');
    }
}
