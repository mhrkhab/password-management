<?php

namespace App\Http\Requests\Users;

use App\Rules\Auth\UniqueUsername;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return request()->session()->exists('user_id');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['required',new UniqueUsername],
            'password' => 'required'
        ];
    }
}
