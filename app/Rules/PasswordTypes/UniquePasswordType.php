<?php

namespace App\Rules\PasswordTypes;

use App\Models\PasswordType;
use Illuminate\Contracts\Validation\Rule;

class UniquePasswordType implements Rule
{
    private $items;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->items = PasswordType::getAll();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $items = array_keys(
            collect($this->items)
                ->keyBy('password_type')
                ->toArray()
        );
        if (in_array($value,$items))
            return false;

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'password type should be unique.';
    }
}
