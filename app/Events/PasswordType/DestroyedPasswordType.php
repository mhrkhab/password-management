<?php

namespace App\Events\PasswordType;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DestroyedPasswordType
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;

    public $password_type_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->user_id = request()->session()->get('user_id');
        $this->password_type_id = $id;
    }
}
