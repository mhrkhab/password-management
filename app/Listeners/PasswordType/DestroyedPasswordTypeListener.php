<?php

namespace App\Listeners\PasswordType;

use App\Events\PasswordType\DestroyedPasswordType;
use App\Models\Password;
use Exception;

/**
 * Class DestroyedPasswordTypeListener
 */
class DestroyedPasswordTypeListener
{

    /**
     * Handle the event.
     *
     * @param DestroyedPasswordType $event
     * @return void
     * @throws Exception
     */
    public function handle(DestroyedPasswordType $event)
    {
        $passwords = Password::getAllMine();
        foreach ($passwords as $password){
            if ($password['password_type_id'] == $event->password_type_id) {
                Password::delete($password['id']);
            }
        }
    }
}
