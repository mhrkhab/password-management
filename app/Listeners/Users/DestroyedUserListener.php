<?php

namespace App\Listeners\Users;

use App\Events\Users\DestroyedUser;
use App\Models\Password;
use App\Models\PasswordType;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

/**
 * Class DestroyedUserListener
 */
class DestroyedUserListener
{

    /**
     * Handle the event.
     *
     * @param DestroyedUser $event
     * @return void
     * @throws FileNotFoundException
     */
    public function handle(DestroyedUser $event)
    {
        $passwords = Password::getAllMine($event->user_id);
        foreach ($passwords as $password){
            Password::delete($password['id'], $event->user_id);
        }

        $passwordTypes = PasswordType::getAllMine($event->user_id);
        foreach ($passwordTypes as $passwordType){
            PasswordType::delete($passwordType['id'], $event->user_id);
        }
    }
}
