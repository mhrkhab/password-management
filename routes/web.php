<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LaunchController;
use App\Http\Controllers\PasswordsController;
use App\Http\Controllers\PasswordTypesController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', [LaunchController::class,'launch'])->name('launch');
Route::post('/login',[AuthController::class,'login'])->name('login');
Route::get('/register',[AuthController::class,'register'])->name('register');
Route::post('/signup',[AuthController::class,'signUp'])->name('signUp');

Route::middleware('has-access-permission')->group(function (){

    Route::prefix('users')->group(function (){
        Route::post('',[UsersController::class,'store'])->name('users.store');
        Route::get('/{id}/destroy',[UsersController::class,'destroy'])->name('users.destroy');
        Route::get('',[UsersController::class,'index'])->name('users.index');
    });

    Route::prefix('passwords')->group(function (){
        Route::post('',[PasswordsController::class,'store'])->name('passwords.store')->middleware('check-password-type');
        Route::prefix('{id}')->group(function (){
            Route::post('/update',[PasswordsController::class,'update'])->name('passwords.update');
            Route::get('/destroy',[PasswordsController::class,'destroy'])->name('passwords.destroy');
        });
        Route::get('/index/{password_type_id}',[PasswordsController::class,'index'])->name('passwords.index')->middleware('check-password-type');

        Route::prefix('types')->group(function (){
            Route::post('',[PasswordTypesController::class,'store'])->name('passwords.types.store');
            Route::get('/{id}/destroy',[PasswordTypesController::class,'destroy'])->name('passwords.types.destroy');
            Route::get('',[PasswordTypesController::class,'index'])->name('passwords.types.index');
        });
    });

});
